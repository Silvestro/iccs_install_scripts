#!/bin/bash
# --------------------------------------
# Knowledge Extractor Build Script
# --------------------------------------
echo "Knowledge Extractor build script"

apt-get update
apt-get install apt-transport-https -y
apt-get install ca-certificates -y
apt-get install curl -y
apt-get install software-properties-common -y
apt-get install git -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce -y
docker run -d --rm -p 8080:8080 tomcat
git clone https://Silvestro@bitbucket.org/Silvestro/knowledge_extractor.git
git clone https://github.com/johnviolos/KnowledgeExtractor4.git
tomcatID=$(docker ps | grep tomcat | awk '{print $1}')
docker cp ./KnowledgeExtractor4/knowledge_base $tomcatID:/
docker exec -d $tomcatID chmod 777 /knowledge_base
docker cp ./knowledge_extractor/knowledge_extractor.war $tomcatID:/usr/local/tomcat/webapps