#!/bin/bash
# --------------------------------------
# Security_Module Build Script
# --------------------------------------
echo "security_module build script"
#install java{
apt-get update
apt-get install -y python-software-properties debconf-utils
add-apt-repository -y ppa:webupd8team/java
apt-get update
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
apt-get install -y oracle-java8-installer
#}

#clone the jar file
cd /
git clone https://Silvestro@bitbucket.org/Silvestro/budamaf_security_jar.git 

#add user and ip forwarding permissions
adduser budamaf_security --gecos "" --disabled-password
sysctl net.ipv4.ip_forward=1
sysctl -w net.ipv4.conf.all.route_localnet=1

#add port forwarding rules
#iptables -P INPUT ACCEPT
#iptables -P FORWARD ACCEPT
#iptables -P OUTPUT ACCEPT
#iptables -t nat -F
#iptables -t mangle -F
#iptables -F
#iptables -X
outSet=false
inSet=false
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
case $key in
    -op|--out-port)
    outbound_port="$2"
    outSet=true
    shift # past argument
    shift # past value
    ;;
    -ip|--in-port)
    inbound_port="$2"
    inSet=true
    shift # past argument
    shift # past value
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters
if ($outSet)
then
echo "outbound_ports $outbound_port"
outbound_ports=$(echo $outbound_port | tr "," "\n")
for port in $outbound_ports
do
    iptables -t nat -I OUTPUT 1 -p tcp --dport $port -j DNAT --to-destination 127.0.0.1:1551
done
iptables -t nat -I OUTPUT 1 -p tcp -m owner --uid-owner budamaf_security -j ACCEPT
fi
if ($inSet)
then
echo "inbound_ports $inbound_port"
inbound_ports=$(echo $inbound_port | tr "," "\n")
for port in $inbound_ports
do
    iptables -t nat -I PREROUTING 1 -p tcp --dport $port -j REDIRECT --to-port 1551
done
iptables -t nat -I PREROUTING 1 -p tcp -s 127.0.0.1 -j ACCEPT
#iptables -t nat -I PREROUTING 1 -p tcp -s 127.0.1.1 -j ACCEPT
fi

#start the servers
homedirectory=~
LOG_DIR=$homedirectory/budamaf_logs
mkdir $LOG_DIR
logout="$LOG_DIR/security_module_$(date +%Y-%m-%d.%H:%M:%S).out"
logerr="$LOG_DIR/security_module_$(date +%Y-%m-%d.%H:%M:%S).err"
nohup runuser -l budamaf_security -c "java -jar /budamaf_security_jar/BUDaMaF_Security_Module.jar \"{'secure_tunnel_port':1551}\"" > $logout 2>$logerr &

